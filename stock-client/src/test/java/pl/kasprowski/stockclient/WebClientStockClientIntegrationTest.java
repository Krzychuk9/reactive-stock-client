package pl.kasprowski.stockclient;

import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;
import pl.kasprowski.stockclient.model.StockPrice;
import reactor.core.publisher.Flux;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class WebClientStockClientIntegrationTest {

    private WebClient webClient = WebClient.builder().build();

    @Test
    void shouldRetrieveStockPricesFromTheService() {
        final WebClientStockClient client = new WebClientStockClient(webClient);

        final Flux<StockPrice> prices = client.pricesFor("symbol");

        assertNotNull(prices);
        Flux<StockPrice> fivePrices = prices.take(5);
        assertEquals(5, fivePrices.count().block());
        assertEquals("symbol", fivePrices.blockFirst().getSymbol());
    }
}
