package pl.kasprowski.stockclient;

import pl.kasprowski.stockclient.model.StockPrice;
import reactor.core.publisher.Flux;

public interface StockClient {
    Flux<StockPrice> pricesFor(final String symbol);
}
