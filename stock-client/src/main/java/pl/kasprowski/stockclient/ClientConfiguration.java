package pl.kasprowski.stockclient;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ClientConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WebClient webClient() {
        return WebClient.builder().build();
    }

    @Bean
    @Profile("sse")
    public StockClient webClientStockClient(final WebClient webClient) {
        return new WebClientStockClient(webClient);
    }

    @Bean
    public RSocketRequester rSocketRequester(final RSocketRequester.Builder builder) {
        return builder.connectTcp("localhost", 7000).block();
    }

    @Bean
    @Profile("rsocket")
    public StockClient rSocketStockClient(final RSocketRequester rSocketRequester) {
        return new RSocketStockClient(rSocketRequester);
    }
}
