package pl.kasprowski.stockui;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import org.springframework.stereotype.Component;
import pl.kasprowski.stockclient.StockClient;
import pl.kasprowski.stockclient.model.StockPrice;

import java.util.function.Consumer;

import static java.lang.String.valueOf;
import static javafx.collections.FXCollections.observableArrayList;

@Component
public class ChartController {

    @FXML
    public LineChart<String, Double> chart;

    private final StockClient client;

    public ChartController(final StockClient client) {
        this.client = client;
    }

    @FXML
    public void initialize() {
        final String symbolOne = "SYMBOL1";
        final PriceSubscriber priceSubscriberOne = new PriceSubscriber(symbolOne, client);

        final String symbolTwo = "SYMBOL2";
        final PriceSubscriber priceSubscriberTwo = new PriceSubscriber(symbolTwo, client);

        final ObservableList<Series<String, Double>> data = observableArrayList();
        data.add(priceSubscriberOne.getSeries());
        data.add(priceSubscriberTwo.getSeries());
        chart.setData(data);
    }

    private static class PriceSubscriber implements Consumer<StockPrice> {

        private final ObservableList<Data<String, Double>> seriesData;
        private final Series<String, Double> series;

        public PriceSubscriber(final String symbol, final StockClient client) {
            this.seriesData = observableArrayList();
            this.series = new Series<>(symbol, seriesData);
            client.pricesFor(symbol)
                    .subscribe(this);
        }

        public Series<String, Double> getSeries() {
            return series;
        }

        @Override
        public void accept(final StockPrice stockPrice) {
            Platform.runLater(() -> {
                final String x = valueOf(stockPrice.getTime().getSecond());
                final Double y = stockPrice.getPrice();
                final Data<String, Double> data = new Data<>(x, y);
                seriesData.add(data);
            });
        }
    }
}
